//============================================================================
// Name        : main.cpp
// Author      : Gani
// Version     :
// Copyright   : MIT
// Description : Solvers for MPlus Tests
//============================================================================

#include <iostream>
#include <vector>
#include <algorithm>
#include "Test01.h"
#include "Test02.h"
#include "Test03.h"
#include "Test04.h"
#include "Test05.h"

using namespace std;

int main() {
	// Test cases Problem 01
	vector<int> t11 {10, 1,2,9,0,4,5,6,7,6,1,3,1,1,1,1,2,1};
	vector<int> t12 {2, 3, 10, 6, 4, 8, 1};
	vector<int> t13 {7, 9, 5, 8, 3, 2};
	vector<int> t14 {1, 2, 2, 1, 0, 0};

	// Test cases Problem 02
	string t21 {"aabcxyzklmaabc"};
	string t22 {"aabcdaabc"};
	string t23 {"abcab"};
	string t24 {"aaaa"};

	// Test cases Problem 03
	string t31 {"aaaaabbbbcccccccdaa"};
	string t32 {"abcdfegh"};
	string t33 {"abcdfeghzzzzzzzzzzz"};

	// Test cases Problem 04
	vector<int> t41 {1,2,147,483,647};

	// Test cases Problem 05
	vector<int> t51(100-1);
	vector<int> t52(5000-1);

	Test01 Test01Solver;
	Test02 Test02Solver;
	Test03 Test03Solver;
	Test04 Test04Solver;
	Test05 Test05Solver;

	cout << "=========================" << endl;
	Test01Solver.execute(t11);
	Test01Solver.execute(t12);
	Test01Solver.execute(t13);
	Test01Solver.execute(t14);
	cout << "=========================" << endl;
	Test02Solver.execute(t21);
	Test02Solver.execute(t22);
	Test02Solver.execute(t23);
	Test02Solver.execute(t24);
	cout << "=========================" << endl;
	Test03Solver.execute(t31);
	Test03Solver.execute(t32);
	Test03Solver.execute(t33);
	cout << "=========================" << endl;
	Test04Solver.execute(t41);
	cout << "=========================" << endl;
	Test05Solver.execute(t51);
	Test05Solver.execute(t52);
	cout << "=========================" << endl;

	return 0;
}
