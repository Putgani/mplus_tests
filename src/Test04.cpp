/*
 * Test04.cpp
 *
 *  Created on: Jan 26, 2020
 *      Author: elektroingenieur
 */

#include "Test04.h"

Test04::Test04() {}
Test04::~Test04() {}

unsigned long Test04::dec2bin(int n)
{
    unsigned long bin = 0;
    int r, i = 1;

    while (n!=0) {
        r = n%2;
        n /= 2;
        bin += r*i;
        i *= 10;
    }
    return bin;
}

unsigned long Test04::calculateLongestGap(string s) {
	if(s.size() < 3) {
		return 0;
	}

	vector<int> idx;
	for(auto it = s.begin(); it < s.end(); ++it) {
		if(*it == '1') {
			idx.push_back(distance(s.begin(), it));
		}
	}

	vector<int>::iterator a = idx.begin();
	vector<int>::iterator b = idx.begin() + 1;
	vector<int> result;

	while(b != idx.end()) {
		result.push_back(*b - *a - 1);
		++a; ++b;
	}

	return (*max_element(result.begin(), result.end()));
}

void Test04::execute(vector<int> v) {

	for(const auto &elem: v) {
		string bin = to_string(dec2bin(elem));
		cout << "Test04 Output: " << elem << "(" << bin << ")"<< " longest gap is "<< calculateLongestGap(bin) << endl;
	}
}
