/*
 * Test03.h
 *
 *  Created on: Jan 26, 2020
 *      Author: elektroingenieur
 */

#ifndef TEST03_H_
#define TEST03_H_

#include <iostream>
#include <string>
#include <list>

using namespace std;

class Test03 {
private:

public:
	Test03();
	~Test03();
public:
	void execute(string s);
};



#endif /* TEST03_H_ */
