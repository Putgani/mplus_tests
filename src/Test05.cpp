/*
 * Test05.cpp
 *
 *  Created on: Jan 26, 2020
 *      Author: elektroingenieur
 */

#include "Test05.h"

Test05::Test05() {}
Test05::~Test05() {}

int gen()
{
    static int i = 0;
    return ++i;
}

int Test05::generator(int N) {
	int accum = N;

	while (N > 0)
	{
	    int digit = N%10;
	    accum += digit;
	    N /= 10;
	}
	return accum;
}

void Test05::execute(vector<int> v) {
	vector<int> numbers;
	vector<int> selfNumbers;
	vector<int>::iterator finder;

    std::generate(v.begin(), v.end(), gen);

    for(const auto &elem: v) {
    	numbers.push_back(generator(elem));
    }

    sort(numbers.begin(), numbers.end());

    for(const auto &x: v) {
    	finder = find(numbers.begin(), numbers.end(), x);
    	if(finder == numbers.end()) selfNumbers.push_back(x);
    }

    int result = accumulate(selfNumbers.begin(), selfNumbers.end(), 0);

    cout << "Test05 Output: " << result << endl;

}
