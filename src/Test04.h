/*
 * Test04.h
 *
 *  Created on: Jan 26, 2020
 *      Author: elektroingenieur
 */

#ifndef TEST04_H_
#define TEST04_H_

#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Test04 {
private:

public:
	Test04();
	~Test04();
public:
	unsigned long dec2bin(int n);
	unsigned long calculateLongestGap(string s);
	void execute(vector<int> v);
};



#endif /* TEST04_H_ */
