/*
 * Test05.h
 *
 *  Created on: Jan 26, 2020
 *      Author: elektroingenieur
 */

#ifndef TEST05_H_
#define TEST05_H_

#include <iostream>
#include <string>
#include <vector>
#include <numeric>

using namespace std;

class Test05 {
private:

public:
	Test05();
	~Test05();
public:
	int generator(int N);
	void execute(vector<int> v);
};



#endif /* TEST05_H_ */
