/*
 * Test02.cpp
 *
 *  Created on: Jan 26, 2020
 *      Author: elektroingenieur
 */

#include "Test02.h"

Test02::Test02() {}
Test02::~Test02() {}

void Test02::execute(string s) {
	string s1(s.begin(), s.begin() + s.size()/2);
	string s2;
	if(s.size() % 2 == 0)
		s2 = string(s.begin() + s.size()/2, s.end());
	else
		s2 = string(s.begin() + s.size()/2 + 1, s.end());

	while(s1.compare(s2)) {
		s1.pop_back();
		s2.erase(s2.begin());
	}

	cout << "Test02 Output: " << s1.size() << endl;
}


