/*
 * Test01.cpp
 *
 *  Created on: Jan 26, 2020
 *      Author: elektroingenieur
 */
#include "Test01.h"

using namespace std;

Test01::Test01() {};
Test01::~Test01() {};

vector<int> Test01::findIncreasingTrend(vector<int> &v) {
	vector<int> buffer;
	auto a = v.begin();
	auto b = v.begin() + 1;
	buffer.push_back(*a);

	if(*a > *b) {
		v.erase(v.begin());
		return buffer;
	}

	while(*a < *b) {
		buffer.push_back(*b);
		if(a++ == v.end()) break;
		b++;
	}

	v = vector<int>(b, v.end());
	return buffer;
}

void Test01::execute(vector<int> v) {
	vector<int> results;

	if(v.size() < 2) cout << "Test01 Output: " << 0 << endl;
	vector<int> temp;

	while(v.size()>1) {
		temp = findIncreasingTrend(v);
		results.push_back(temp.back() - temp.front());
	}

	cout << "Test01 Output: " << *max_element(results.begin(), results.end()) << endl;
}
