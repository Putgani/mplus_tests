/*
 * Test02.h
 *
 *  Created on: Jan 26, 2020
 *      Author: elektroingenieur
 */

#ifndef TEST02_H_
#define TEST02_H_

#include <iostream>
#include <string>

using namespace std;

class Test02 {
private:

public:
	Test02();
	~Test02();
public:
	void execute(string s);
};



#endif /* TEST02_H_ */
