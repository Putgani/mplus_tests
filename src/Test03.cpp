/*
 * Test03.cpp
 *
 *  Created on: Jan 26, 2020
 *      Author: elektroingenieur
 */

#include "Test03.h"

Test03::Test03() {}
Test03::~Test03() {}

void Test03::execute(string s) {

	string result;
	size_t counter = 0;
	size_t pos = 0;

	string::iterator it = s.begin();
	while(it != s.end()) {
		if(*it == s[pos]) {
			it++;
			counter++ ;
			if(it == s.end()) {
				result.push_back(s[pos]);
				result.append(to_string(counter));
				break;
			}
		}
		else {
			result.push_back(s[pos]);
			result.append(to_string(counter));
			s.erase(s.begin(), s.begin() + counter);
			it = s.begin();
			counter = 0;
		}
	}
	cout << "Test03 Output: " << result << endl;

}
