/*
 * Test01.h
 *
 *  Created on: Jan 26, 2020
 *      Author: elektroingenieur
 */

#ifndef TEST01_H_
#define TEST01_H_

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Test01 {
private:

public:
	Test01();
	~Test01();
public:
	vector<int> findIncreasingTrend(vector<int> &v);
	void execute(vector<int> v);
};


#endif /* TEST01_H_ */
